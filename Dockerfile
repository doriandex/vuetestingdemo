FROM node:latest
MAINTAINER DorianDex

COPY . .

ENV PATH /app/node_modules/.bin:$PATH

RUN npm install --progress=false 
RUN npm run build


EXPOSE 8080