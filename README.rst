====================================
Frontend Vuetesting
====================================

Frameworks etc.
---------------

* Vue.js 2
* Mocha and Chai for unittesting 4.2
* Nightwatch for e2e testing 1.2.2
...

Basics
------------

This repo contains democode for testing a Vue.js App:

* Nightwatch E2E Tests
* Mocha and gChai Unittests
* gitlab-ci.yaml
* Dockerfile
* Docker-compose file


Setup
------

Install project and run it:

* npm install
* npm run serve
* npm run build

