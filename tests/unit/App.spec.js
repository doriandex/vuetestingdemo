import { expect } from 'chai';
import { shallowMount } from "@vue/test-utils";
import App from "@/App.vue";

describe('App.vue', () => {
  it('should pass this demo test', () => {
     expect('Demo').to.equal('Demo');
  });
});
describe('App.vue', () => {
  const wrapper = shallowMount(App, { })
  it('1 + 2 = 3', () => {
     expect(wrapper.vm.itemTotal(1,2)).to.equal(3);
  });
});
