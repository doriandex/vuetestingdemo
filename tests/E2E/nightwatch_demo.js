module.exports = {
    'My first demo test case'(browser) {
        browser
            .url('http://vue-e2e:8080')
            .waitForElementVisible('body')
            .source(function(result){console.log(result.value)}) //this will print the whole HTML code of your page for debugging etv.!!!
            browser.assert.elementPresent('.headline')
            .assert.containsText('.headline', 'Testwebsite');
    }
  };
  